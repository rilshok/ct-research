import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def plot_mask_slices(predict_cude, target_cude, step,):
    predict_cude = predict_cude[0,0].detach().cpu().numpy()
    target_cude = target_cude[0,0].detach().cpu().numpy()
    number_of_layers = predict_cude.shape[0]
    number_of_image = round(number_of_layers/step)
    fig , axes = plt.subplots(number_of_image,2, figsize=(6,2*number_of_image))
    start = 0
    for i in range(number_of_image):
        axes[i,0].imshow(predict_cude[start,:,:])
        axes[i,1].imshow(target_cude[start,:,:])
        start+=step
        axes[i,0].set_title('Слой {}'.format(start))
        axes[i,1].set_title('Слой {}'.format(start))
    plt.tight_layout(w_pad=0.01, h_pad=0.01)
    plt.savefig('mask.jpeg')