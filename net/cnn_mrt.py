import os
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from plot_tools import plot_mask_slices
from torch.utils.data import DataLoader
from model import resnet18
from ExGen import ExampleGenerator, segmentation

from ct_lossFunctions import DiceLoss, LossChecker

class CT_Network_Creater:
    def __init__(self, grid, volume, dataset_path):
        """
        :grid: Сетка подаваемая на вход нейросети
        :volume: Объем в милиметрах обрабатываемый нейросетью
        :dataset_path:
            В дирректории dataset_path должны быть представленны 
            пары файлов имеющие такой именование:
            i<NUMBER>.nii ; o<NUMBER>.nii
            Напрмиер дирректория может содержать такие файлы:
            i1.nii; o1.nii; i132.nii; o132.nii
        """
        self.device = 'cpu'
        if torch.cuda.is_available():
            self.device = 'cuda'

        self._GRID=np.array(grid)
        self._VOLUME=np.array(volume)
        #Создаем массив генераторов
        self.__create_dataloader(dataset_path)
    def __create_dataloader(self, dataset_path, number_of_generators=-1):
        example_paths=[]
        files=[]
        try:
            files = os.listdir(dataset_path)
        except FileNotFoundError:
            raise FileNotFoundError("Неверный путь к дирректории с обучающими примерами: " + dataset_path)
        input_files  = sorted(list(filter(lambda x: x.split('.')[0].find('i') > -1, files)))
        target_files = sorted(list(filter(lambda x: x.split('.')[0].find('o') > -1, files)))
        for in_f in input_files:
            out_f='o'+in_f.split('.')[0][1:]+".nii"
            if out_f in target_files:
                example_paths.append([dataset_path+in_f,dataset_path+out_f])
        if len(example_paths)==0:
            raise FileNotFoundError("В дирректории " + dataset_path +" нет файлов с обучающими примерами")
        self.generators=[]
        for example_path in example_paths:
            if number_of_generators==0: break
            number_of_generators-=1
            gen=ExampleGenerator(
                input_path     =example_path[0],
                response_path  =example_path[1],
                volume         =self._VOLUME,
                grid           =self._GRID,
                distortion     =(self._VOLUME[0]*self._VOLUME[1]*self._VOLUME[2])**(1/3)*0.08
                )
            #Обработать ошибку нехватки памяти
            self.generators.append(gen)
    def Get_Dataloader_generator(self,prob=0.5):
        def dataloader_generator(number_of_example,batch_size,num_workers):
            dataset = []
            for e in range(number_of_example):
                gen_idx=np.random.randint(len(self.generators))
                i,o,_ = self.generators[gen_idx](prob)
                dataset.append((torch.tensor(i[np.newaxis, :]), torch.tensor(o[np.newaxis, :])))
            dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers = num_workers)

            return dataloader
        return dataloader_generator
    def Get_NewNetwork_and_Trainer(self):
        net = resnet18().to(self.device)

        def trainer(epochs,number_of_mini_epoch,number_of_example,batch_size,num_workers,lr,loss_history,prob_positive_gen):
            """
            :epochs: Число эпох
            :number_of_mini_epoch: Число подъэпох
            :number_of_example: Число генерируемых примеров для обучения, для валидации 2*number_of_example
            :batch_size: Размер батча
            :num_workers: Число потоков для dataloader
            :lr: Скорость обучения
            :loss_history: Словарь истории обучения loss_history = {'train': [], 'validation' : []}
            :prob_positive_gen: - Вероятность генерации положительного примера
            """
            losscheck=LossChecker(50)
            optimizator = optim.Adam(net.parameters(), lr = lr)
            def change_lr(lr):
                for g in optimizator.param_groups:
                    g['lr'] = lr
            criterion = DiceLoss().to(self.device)

            validation=self.Get_Dataloader_generator(0.5)(number_of_example*2,batch_size,num_workers)

            generator_of_dataloaders=self.Get_Dataloader_generator(prob_positive_gen)
            
            for epoch in range(epochs):
                dataloader=generator_of_dataloaders(number_of_example,batch_size,num_workers)
                loss_on_batch = np.array([])
                for mini_epoch in range(number_of_mini_epoch):
                    for inpt, target in dataloader:
                        inpt = inpt.to(self.device)
                        target = target.to(self.device)
                        optimizator.zero_grad()
                        out = net(inpt)
                        loss = criterion(out, target)
                        loss_on_batch=np.append(loss_on_batch,loss.item())
                        loss.backward()
                        optimizator.step()
                loss_on_batch=loss_on_batch.mean()

                loss_history['train'].append(loss_on_batch)
                del dataloader
                if losscheck(loss_on_batch):
                    lr=lr*0.5
                    change_lr(lr)
                    print("LR changed:",lr)
                if lr<1e-6:
                    break

                #Ошибка волидационного набора
                loss_on_epoch_val=[]
                for validation_inpt, validation_target in validation:
                    validation_inpt = validation_inpt.to(self.device)
                    validation_target = validation_target.to(self.device)
                    validation_out = net(validation_inpt)
                    validation_loss = criterion(validation_out, validation_target)
                    loss_on_epoch_val.append(validation_loss.item())
                loss_on_epoch_val=np.array(loss_on_epoch_val).sum()/len(loss_on_epoch_val)
                print('Epoch:{}\t Loss: {}'.format(epoch+1, loss_on_epoch_val))
                loss_history['validation'].append(loss_on_epoch_val)
            return 

        return net, trainer

class CT_Network_Manager:
    def __init__(self):
        self.device = 'cpu'
        if torch.cuda.is_available():
            self.device = 'cuda'        
        self.net = resnet18().to(self.device)
    def save_network(self,path_to_network):
        torch.save(self.net.state_dict(), path_to_network)
        
    def load_network(self,path_to_network):
        state = torch.load(path_to_network, map_location=torch.device('cpu'))
        self.net.load_state_dict(state)
    
    def processing_nii(self, input_path, output_path, volume, grid):
        def func(data):
            tensor_data = torch.tensor(data[np.newaxis, np.newaxis,:]).to(self.device)
            return np.rint(self.net(tensor_data)[0,0].detach().cpu().numpy())
        segmentation(input_path, output_path, volume, grid, func)
    def validate_nii(self):
        pass

        

if __name__ == "__main__":
    DATASET_PATH="./trainset/"
    LOCAL_PATH="."
    grid=(64,64,64)
    
    volume=(60,60,80)
    Net_creater=CT_Network_Creater(grid, volume, DATASET_PATH)

    history = {'train': [], 'validation' : []}
    net,trainer=Net_creater.Get_NewNetwork_and_Trainer()

    trainer(10, 5, 2, 1, 2,0.01, history, 0.25)
    torch.save(net.state_dict(), LOCAL_PATH+'local_network')

    # Net_creater=CT_Network_Creater(grid=(64,64,64), volume=(40,40,130),dataset_path=path_to_dataset)
    
    # net,trainer=Net_creater.Get_NewNetwork_and_Trainer()
    # history=trainer(40,2,2,2,0.1)
    # for h in history:
    #     print(h)
    

    #Для тестовой сегментации
    # network_manager = CT_Network_Manager()
    # network_manager.load_network('colab_network')
    # network_manager.processing_nii("./trainset/i3.nii","s3_60x60x80.nii",volume,grid)



# print('E:{}\t Loss: {},'.format(epoch, loss.item()), end='\r', flush=True)


def jaccard_score_numpy(y_true, y_pred):
    assert y_true.ndim == y_pred.ndim
    assert not np.isnan(y_true).any()
    assert not np.isnan(y_pred).any()
    y_true = y_true.flatten()
    y_pred = y_pred.flatten()
    intersection = y_true@y_pred
    union = y_true.sum() + y_pred.sum() - intersection + 1e-6
    return intersection/(union)


def validation_score(net, validation_dataset, device):
    number_examples = len(validation_dataset)
    s=0
    for inpt_example, y_target in validation_dataset:
        # y_predict = net(inpt_example.unsqueeze(0).to(device))[0].detach().cpu().numpy()
        y_predict = net(inpt_example.to(device)).detach().cpu().numpy()
        print(y_predict)
        score = jaccard_score_numpy(y_target.detach().numpy(), np.round(y_predict))
        s+=score
    return s/number_examples
