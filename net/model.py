import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch.autograd import Variable
from functools import partial

__all__ = [
    'resnet18'
]


def conv_3d(in_channels, out_channels, kernel_size=3, stride=1, padding=1):
    return nn.Conv3d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding, bias=False)

def conv_trans_3d(in_channels, out_channels, kernel_size = 3, stride = 1):
    return nn.ConvTranspose3d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=1, bias=False)



#Уменьшает размерность входа в ResNet блоке
def downsample_basic_block(x, planes, stride):
    out = F.avg_pool3d(x, kernel_size=1, stride=stride) #Забирает элемент из тензора с шагом stride
    zero_pads = torch.Tensor(
        out.size(0), planes - out.size(1), out.size(2), out.size(3),
        out.size(4)).zero_()
    if isinstance(out.data, torch.cuda.FloatTensor):
        zero_pads = zero_pads.cuda()

    out = Variable(torch.cat([out.data, zero_pads], dim=1))

    return out

class DecoderBlock(nn.Module):
    def __init__(self, in_channels, middle_channels, out_channels):
        super(DecoderBlock, self).__init__()
        self.block = nn.Sequential( conv_3d(in_channels, middle_channels),
                                    nn.ReLU(inplace = True),
                                    conv_trans_3d(middle_channels, out_channels, kernel_size=4, stride=2),
                                    nn.BatchNorm3d(out_channels),
                                    nn.ReLU(inplace=True)
                                    )   
    def forward(self, x):
        return self.block(x)

class DecoderBlockWithSigmoid(nn.Module):
    def __init__(self, in_channels, middle_channels, out_channels):
        super(DecoderBlockWithSigmoid, self).__init__()
        self.block = nn.Sequential( conv_3d(in_channels, middle_channels),
                                    nn.ReLU(inplace = True),
                                    conv_trans_3d(middle_channels, out_channels, kernel_size=4, stride=2),
                                    nn.BatchNorm3d(out_channels),
                                    # nn.ReLU(inplace=True)
                                    nn.Sigmoid()
                    
                                    )   
    def forward(self, x):
        return self.block(x)                                 

class ConvolutionBlock(nn.Module):
    """
        X(K)
          |       \
          |         \
          |        [3x3x3]
          |           |
                 [Batch_Norm]
                      |  
                  [LekyRelu]
                      |
                   [3х3х3]
                      |
                  [LekyRelu]
                       
    """
    expansion = 1
    def __init__(self, in_channels, out_channels, stride=1, downsample=None, negative_slope = 0.1):
        super(ConvolutionBlock, self).__init__()
        self.conv1 = conv_3d(in_channels, out_channels, kernel_size = 3, stride = stride)
        self.bn1 = nn.BatchNorm3d(out_channels)
        self.activation = nn.LeakyReLU(negative_slope, inplace = True)
        self.conv2 = conv_3d(out_channels, out_channels)
        self.bn2 = nn.BatchNorm3d(out_channels)
        self.downsample = downsample

    def forward(self, x):
        residual = x
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.activation(out)
		
        out = self.conv2(out)
        out = self.bn2(out)
		
        if self.downsample is not None:
            # #print('до',x.shape)
            residual = self.downsample(x)
        # #print('res:', residual.shape, 'out:', out.shape)
        out += residual
        return self.activation(out)

class ResNet3d(nn.Module):
    def __init__(self, block, layers):
        super(ResNet3d, self).__init__()
        self.in_planes = 64
    
        self.conv1 = nn.Conv3d(1,64, kernel_size=4, stride=2, padding=1, bias = False)
        self.bn1 = nn.BatchNorm3d(64)
        self.activation = nn.LeakyReLU(0.1, inplace=True)
        self.maxpool = nn.MaxPool3d(kernel_size=3, stride=2, padding=1)

        self.layer1 = self._make_layer(block,  64, layers[0], stride = 1)
        self.layer2 = self._make_layer(block, 128, layers[1], stride = 2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride = 2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride = 2)
        self.pool   = nn.MaxPool3d(4,2, padding=1)
        self.bridge = DecoderBlock(512, 256, 256)
        self.dec5   = DecoderBlock(512 + 256, 256, 128)
        self.dec4   = DecoderBlock(256 + 128, 128, 64)
        self.dec3   = DecoderBlock(128 + 64, 64, 32)
        self.dec2   = DecoderBlock(64 + 32, 32, 16)
        self.dec1   = DecoderBlockWithSigmoid(64 + 16, 8, 1)

    def forward(self, x):
        conv1 = self.conv1(x)
        #print('Conv_1: ', conv1.shape)
        conv1_bn = self.bn1(conv1)
        #print('Conv_1_BN: ', conv1_bn.shape)
        conv1 = self.activation(conv1_bn)
        #print('Conv1+BN+Act: ', conv1.shape)
        # conv1_pool = conv1
        conv1_pool = self.maxpool(conv1)
        #print('Conv1+pool: ', conv1_pool.shape)

        conv2 = self.layer1(conv1_pool)
        #print('Conv2: ', conv2.shape)
        conv3 = self.layer2(conv2)
        #print('Conv3: ', conv3.shape)
        conv4 = self.layer3(conv3)
        #print('Conv4: ', conv4.shape)
        conv5 = self.layer4(conv4)
        #print('Conv5: ', conv5.shape)

        # pool  = conv5 
        pool  = self.pool(conv5)
        #print('Pool: ', pool.shape)
        center = self.bridge(pool)
        #print('Center: ', center.shape)
        dec5   = self.dec5(torch.cat([center, conv5], 1))
        #print('dec5: ',dec5.shape)
        dec4   = self.dec4(torch.cat([dec5, conv4], 1))
        #print('dec4: ',dec4.shape, conv3.shape)
        dec3   = self.dec3(torch.cat([dec4, conv3], 1))
        #print('dec3: ',dec3.shape, conv2.shape)
        dec2   = self.dec2(torch.cat([dec3, conv2], 1))
        #print('dec2: ',dec2.shape, conv1.shape)
        dec1   = self.dec1(torch.cat([dec2, conv1], 1))
        # print('dec1: ',dec1.shape, dec1)

        return dec1


    def _make_layer(self, block, planes, num_blocks, stride):
        """
            Собрать слой на основе базового conv3x3x3
        """
        downsample = None
        if stride != 1 or self.in_planes != planes * block.expansion:
            # downsample = partial( downsample_basic_block, planes=planes * block.expansion, stride=stride)	
            downsample = nn.Sequential(
                    nn.Conv3d( self.in_planes, planes * block.expansion, kernel_size=1, stride=stride, bias=False), nn.BatchNorm3d(planes * block.expansion))
        layers = []
        layers.append(block(self.in_planes, planes, stride, downsample))
        self.in_planes = planes * block.expansion
        
        for _ in range(1, num_blocks):
            layers.append(block(self.in_planes, planes))    

        return nn.Sequential(*layers)

def resnet18(**kwargs):
    """Constructs a ResNet-18 model.
    """
    model = ResNet3d(ConvolutionBlock, [2, 2, 2, 2], **kwargs)
    return model


def resnet34(**kwargs):
    """Constructs a ResNet-34 model.
    """
    model = ResNet3d(ConvolutionBlock, [3, 4, 6, 3], **kwargs)
    return model
