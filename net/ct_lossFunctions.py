import torch
import torch.nn as nn
import numpy as np
class DiceLoss(nn.Module):
    """
        prediction (P): Предсказываемая разметка
        target     (T): Целевая разметка
        Функция Даеса.
        Находит отношение пересечения A и T к объединению A и T.
        Результат вычитается из единицы с целью использования при оптимизации.
    """
    def __init__(self):
        super().__init__()
    def forward(self, prediction, target):
        assert prediction.size() == target.size()
        prediction = prediction.view(-1)
        target = target.view(-1)
        intersection = (prediction*target).sum()
        # union=(prediction*prediction).sum() + (target*target).sum() + 1e-6
        # dcs = (2. * intersection)/union
        union=((1-prediction)*target).sum()+(prediction*(1-target)).sum()+intersection + 1e-6
        return 1-intersection/union

# Функция с очень сомнительной ценностью.
#Объеденяет функцию Даеса и бинарную кросс энтропию
class Dice_BCE_Loss(nn.Module):
    """
          Для бинарной маски
          return:  -weight*log(dice) + (1 - weight)*bce
    """
    def __init__(self, weight=0.5, gamma_d = 0.3, gamma_b = 0.3):
        super().__init__()
        self.weight = weight
        self.dice_loss = DiceLoss()
        self.bce_loss  = nn.BCELoss()
        
    def forward(self, y_pred, y_target):
        assert not (y_target > 1).any().item() or not(y_pred > 1).any().item()
        dice = self.dice_loss(y_pred, y_target)
        bce  = self.bce_loss(y_pred, y_target)
        return -self.weight*torch.log(dice) + (1. - self.weight)*bce


class LossChecker:
    def __init__(self, freeze):
        self.freeze=freeze
        self.arr=np.array([])
        
    def __call__(self, value):
        self.arr=np.append(self.arr,value)
        L=len(self.arr)
        if L<self.freeze:
            return False
        big   = self.arr[-int(2*L/3):].mean()
        small = self.arr[-int(L/3):].mean()
        if small>big:
            self.arr=np.array([])
            return True
        return False