# Dataset
Download dataset from: [Яндекс Диск](https://yadi.sk/d/UtPbELulUUXNBg)

| Input  | Output |
|--------|--------|
|i1.nii  |o1.nii  |
|i2.nii  |o2.nii  |
|i3.nii  |o3.nii  |
